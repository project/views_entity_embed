# Views Conditional

Views entity embed module allows you to embed views in a textarea using
WYSIWYG editor.

For a full description of the module, visit the
[project page](https://drupal.org/project/views_entity_embed)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://drupal.org/project/issues/views_entity_embed)

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

  - Editor and Filter or Link modules included with Drupal core.
  - [Embed module](https://www.drupal.org/project/embed)
  - [Entity Embed module](https://www.drupal.org/project/entity_embed)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to Administration » Configuration » Content authoring » Text formats
   and editors
1. Enable the filter ‘Display embedded views’ for the desired text
  formats. If the Limit allowed HTML tags filter
  is enabled, add <drupal-views data-view-* data-embed-button> to
  the Allowed HTML tags.
1. Go Administration » Configuration » Content authoring » Embed buttons
   and create a new button, which embedded type is ‘Views’. You can
  choose between the both options:

    - Filter which Views to be allowed as options:
    - Filter which Display to be allowed as options:

Only the selected views will be allowed to be embedded by this Views embed
button and for the Views Displays. If you leave the options
unchecked, all the views will be allowed.

  - To enable the WYSIWYG plugin, move the views-entity-embed button
    into the Active toolbar for the desired text formats. In the WYSIWYG
    follow these steps to embed a view:

      - Click the Views Embed Button
      - Select the view
      - Select the display
      - Checkbox for override the title
      - Populate the context filters (if applicable to the view)

## Maintainers

- Shelane French - [shelane](https://www.drupal.org/u/shelane)
- Milka Petkova - [bibishani](https://www.drupal.org/u/bibishani)
- Nikolay Ignatov - [skek](https://www.drupal.org/u/skek)
